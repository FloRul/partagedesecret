/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

import java.util.ArrayList;

/**
 *
 * @author fr456516
 */
public class Methode1 implements ITraitement{
    /**
     * 
     * @param s le secret 
     * @return un tableau d'octets contenant les caractères du message
     */
    
    public ArrayList<Byte> code(Secret s, int nbP) {
        String temp = s.getContenu();
        ArrayList<Byte> res = new ArrayList<>();
        for(int i = 0; i<temp.length()%nbP; i++){
            res.add(new Byte((byte)0));
        }
        for(int i = 0; i<temp.length(); i++){
            byte val =(byte) temp.codePointAt(i) ;
            res.add(new Byte(val));
        }
        return res;
    }
    
    /**
     * 
     * @param tab le tableau d'octet des charactères du secret
     * @return la chaine correspondante
     */
    @Override
    public String reconstruction(ArrayList<Personne> tab) {
        String s = "";
        for(Personne i : tab){
            byte[] b = i.getPartieMes();
            for(byte by : b){
                s += (char) by;
                
            }
        }
        return s;
    }

    @Override
    public ArrayList<Personne> decoupage(Secret s, int nbP) {
        ArrayList<Byte> messageaDecoupe = this.code(s,nbP);
        int partitionSize = messageaDecoupe.size()/nbP;
        ArrayList<Personne> p = new ArrayList<>();
        for(int i = 0; i<nbP; i++){
            byte[] partition = new byte[partitionSize];
            for(int j = 0; j<partitionSize; j++){
                partition[j] = messageaDecoupe.get(j);
                
            }
            p.add(new Personne(partition));
            for(int j = 0; j<partitionSize; j++){
                messageaDecoupe.remove(0);
            }
            
        }
        return p;
    }
    
}
