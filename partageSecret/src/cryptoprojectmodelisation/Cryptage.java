/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

import java.util.ArrayList;

/**
 *
 * @author ll809273
 */
public class Cryptage {

    private int nbPersonne;
    private Secret message;
    ITraitement methode;
    ArrayList<Personne> parties;
    /***
     * Appel la méthode qu'on souhaite utiliser pour partager le secret
     * @param m Secret qu'il faut partager
     * @param nbPer nombre de personne se partageant le message
     * @param nbPerN nombre de personne necessaire à la reconstution du message
     */
    public Cryptage(String m, int nbPer,int nbPerN, int methode ) {
        this.nbPersonne = nbPer;
        this.message = new Secret(m);   
        switch(methode){
            case 1: this.methode = new Methode1(nbPersonne, nbPerN); break;
            case 2: this.methode = new Methode2(); break;
            case 3: this.methode = new Methode3(); break;
        }
    }
    /**
     * Permet de voir les différentes partie du secret de chaque personne 
     */
    public void AfficherLesParties(){
        int compteur =0;
        for(Personne p : parties){
            
           byte[] b = p.getPartieMes();
           for(byte by : b){
               System.out.println("p :"+compteur);
               System.out.println((int)by);
           }
           compteur +=1;
       }
    }
    
    public void Crypter(){       
        parties =  this.methode.decoupage(message);
    }
    
    public String Decrypter(){
        return this.methode.reconstruction(parties);
    }
    
}
