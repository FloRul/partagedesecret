/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

/**
 *
 * @author fr456516
 */
public class Personne {
    private byte[] partieMes;

    /**
     * renvoie le morceau de secret que possède la personne
     * @return 
     */
    public byte[] getPartieMes() {
        return partieMes;
    }
    
    /**
     * constructeur d'une personne directement avec le morceau de secret qu'elle possède
     * @param portionMessage morceau de secret
     */
    public Personne(byte[] portionMessage){
        partieMes = portionMessage;
    }

    /**
     * constructeur d'une personne par copie
     * @param p Personne qu'on copie
     */
    public Personne(Personne p) {
        this.partieMes = p.getPartieMes();
    }
    
}
