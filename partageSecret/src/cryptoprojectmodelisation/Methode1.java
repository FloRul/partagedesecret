/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

import java.util.ArrayList;

/**
 *
 * @author fr456516
 */
public class Methode1 implements ITraitement{

    private int nbPersonne;
    private int nbPersonneN;
    public Methode1(int nbPers, int nbPersN) {
        nbPersonne = nbPers;
        nbPersonneN = nbPersN;
    }
    /**
     * Permet de transformer le secret (un String) en un tableau de Byte 
     * et met la taille du secret en une taille partageable entre le x Personne necessaire pour reconstruire le secret
     * @param s le secret 
     * @return un tableau d'octets contenant les caractères du message
     */
    
    public ArrayList<Byte> code(Secret s) {
        String temp = s.getContenu();
        ArrayList<Byte> res = new ArrayList<>();
        //ajuste la taille du secret
        System.out.println(temp.length()%nbPersonneN);
        if(temp.length() < nbPersonneN){
            for(int i = 0; i<temp.length()%nbPersonneN; i++){
                res.add(new Byte((byte)0));
            }
        }else if(temp.length()>nbPersonneN){
            int taille = temp.length();
            while(taille%nbPersonneN != 0){
                res.add(new Byte((byte)0));
                taille +=1;
            }
        }
        //convertit en Byte
        for(int i = 0; i<temp.length(); i++){
            byte val =(byte) temp.codePointAt(i) ;
            res.add(new Byte(val));
        }
        return res;
    }
    
    /**
     * Reconstruit le secret à partir des personnes 
     * @param tab le tableau d'octet des charactères du secret
     * @return la chaine correspondante
     */
    @Override
    public String reconstruction(ArrayList<Personne> tab) {
        String s = "";
        for(Personne i : tab){
            byte[] b = i.getPartieMes();
            for(byte by : b){
                s += (char) by;
                
            }
        }
        return s;
    }

    /**
     * divise le secret et donne chaque morceau entre les différente personne
     * @param s secret qu'il faut partager
     * @return le tableau de personne possédant un bout de secret
     */
    @Override
    public ArrayList<Personne> decoupage(Secret s) {
        ArrayList<Byte> messageaDecoupe = this.code(s); // récupère le secret à la bonne taille et dans le bon type
        int partitionSize = messageaDecoupe.size()/nbPersonneN; // détermine la taille du morceau que possède chaque personne
        ArrayList<Personne> p = new ArrayList<>();
        //donne à chaque personne nécessaire un bout différent du secret
        for(int i = 0; i<nbPersonneN; i++){
            byte[] partition = new byte[partitionSize];
            for(int j = 0; j<partitionSize; j++){
                partition[j] = messageaDecoupe.get(j);
                
            }
            p.add(new Personne(partition));
            for(int j = 0; j<partitionSize; j++){
                messageaDecoupe.remove(0);
            }           
        }
        int compteur=0;
        // donne un bout de secret au personne non necessaire à la reconstitution du secret
        for (int i= nbPersonneN+1; i<=nbPersonne;i++){           
            p.add(new Personne(p.get(compteur)));
            compteur+=1;
        }
        return p;
    }
    
}
