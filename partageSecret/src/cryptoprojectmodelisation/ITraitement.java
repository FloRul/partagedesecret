/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

import java.util.ArrayList;

/**
 *
 * @author fr456516
 */
public interface ITraitement {
    public ArrayList<Personne> decoupage(Secret s);
    public String reconstruction(ArrayList<Personne> tab);
}
