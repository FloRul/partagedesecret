/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptoprojectmodelisation;

/**
 * classe représentant le message qui est le secret
 * @author fr456516
 */
public class Secret{
    private String Contenu;

    /**
     * Constructeur
     * @param Contenu message du secret
     */
    public Secret(String Contenu) {
        this.Contenu = Contenu;
    }

    /**
     * revoit le contenu du secret
     * @return secret
     */
    public String getContenu() {
        return Contenu;
    }

    public void setContenu(String Contenu) {
        this.Contenu = Contenu;
    }

}


